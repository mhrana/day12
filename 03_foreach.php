<?php

$name = [
    "firstname" => 'John',
    "lastname" => 'Doe',
    "middlename" => 'Bray'
];
echo "<ul>";
foreach ($name as $key => $value){
    echo "<li> {$key} : {$value}</li>";
};
echo "</ul>";